<?php
namespace App\Controllers;

use \App\Models\Jugador;
use \App\Models\TipoJugador;
require_once '../app/models/Jugador.php';
/**
*
*/
class JugadorController
{

    function __construct()
    {

    }

    public function index(){
        $pagesize = 3;
        $jugadores = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();
        $pages = ceil ($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }

            $jugadores = Jugador::all();
            //$tipo = Jugador::tipo();
            require "../app/views/jugadores/index.php";
    }


    // public function show($arguments)
    // {
    //     $id = (int) $arguments[0];
    //     $user = User::find($id);
    //     require "../app/views/user/show.php";
    // }

    public function register()
    {
        $tipoJugador = TipoJugador::all();
        require '../app/views/jugadores/register.php';
    }

    public function store()
    {

        $tipoJugador = TipoJugador::findTipo($_REQUEST['id_puesto']);
        //var_dump($tipoJugador);
        $id = $tipoJugador->id;
        $jugador = new Jugador();
        //$jugador->id = $id;
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['id_puesto'];
        $jugador->insert();
        header('Location:/jugador');
    }
}
