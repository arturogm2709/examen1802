<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>
  <a href="/jugador/register">Registrar jugador</a>


  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de usuarios</h1>

      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Fecha nacimiento</th>
            <th>Puesto</th>

          </tr>
        </thead>
        <tbody>
          <?php foreach ($jugadores as $jugador): ?>
            <tr>
              <td><?php echo $jugador->id ?></td>
              <td><?php echo $jugador->nombre ?></td>
              <td><?php echo $jugador->date->format('d-m-y') ?></td>
              <td><?php echo $jugador->type->nombre; ?></td>
              <td>
                <a class="btn btn-primary" href="/jugador/titulares/<?php echo $jugador->id ?>">Titular</a>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
      <?php for ($i=1; $i <= $pages ; $i++) { ?>
      <?php if ($i != $page):?>
        <a href="/jugador?page=<?php echo $i ?>" class="btn">
          <?php echo $i ?>
        </a>
      <?php else:  ?>
        <span class="btn">
          <?php echo $i ?>
        </span>
      <?php endif ?>
      <?php } ?>
      <hr>


    </div>

  </main>
  <?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
