<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de producto</h1>

      <form method="post" action="/jugador/store">

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control">
        </div>
                <div class="form-group">
          <label>Fecha de nacimiento</label>
          <input type="datetime-local" name="nacimiento" value="2018-06-12" min="1950-06-07" max="2018-06-14" class="form-control">
        </div>
        <div class="form-group">
         <label>Tipo de jugador</label>
          <select name="id_puesto" class="form-control" value="<?php echo $jugador->nombre ?>">
           <?php foreach ($tipoJugador as $jugador): ?>

            <option value="<?php echo $jugador->id ?>" > <?php echo $jugador->nombre?>

            </option>

          <?php endforeach ?>


        </select>
      </div>
      <button type="submit" class="btn btn-default" name="enviar">Enviar</button>
    </form>
    <hr>
    <a href="/jugador" class="btn btn-primary">Volver a jugadores</a>
  </div>
</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
