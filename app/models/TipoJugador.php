<?php
/**
*
*/
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\Jugador;

//require_once '../core/Model.php';
//require_once '../app/models/Product.php';
/**
*
*/
class TipoJugador extends Model
{

    function __construct()
    {

    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }

    public function jugadores()
    {
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE id_puesto = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;

    }
    public function all()
    {
        $db = Jugador::db();
        $statement = $db->query('SELECT * FROM puestos');
        $tipoJugador = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $tipoJugador;
    }

    public function find($id)
    {
        $db = TipoJugador::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, TipoJugador::class);
        $tipoJugador = $stmt->fetch(PDO::FETCH_CLASS);
        return $tipoJugador;
    }
    public function findTipo($id)
    {
        $db = TipoJugador::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, TipoJugador::class);
        $tipoJugador = $stmt->fetch(PDO::FETCH_CLASS);
        return $tipoJugador;
    }


}
