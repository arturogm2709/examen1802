<?php
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\TipoJugador;
//require_once '../core/Model.php';
/**
*
*/
class Jugador extends Model
{

    function __construct()
    {
        //$this->dateOld = strtotime($this->nacimiento);
        $this->date = new \DateTime($this->nacimiento);
    }

    public static function all()
    {
        $db = Jugador::db();
        $statement = $db->query('SELECT * FROM jugadores');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;
    }
        public function type()
    {

        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $jugador = $statement->fetchAll(PDO::FETCH_CLASS, TipoJugador::class)[0];
        return $jugador;
    }


    public static function find($id)
    {

        $db = Jugador::db();
        $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $jugador = $stmt->fetch(PDO::FETCH_CLASS);
        return $jugador;
    }
        public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }
        public function jugadores()
    {

        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE id_puesto = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;

    }
 public function paginate($size=10){
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }
        $offset = ($page - 1) * $size;

        //$page = $_REQUEST['page'];
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $jugadores;
    }
    public static function rowCount(){
        $db = Jugador::db();
        $statement = $db->prepare('SELECT count(id) AS count FROM jugadores');
        //$statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        //$statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        //$rowCount = $rowCount['count'];

        //$users = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $rowCount['count'];
    }



    public function insert()
    {
        $db = Jugador::db();
        $stmt = $db->prepare('INSERT INTO jugadores( nombre, nacimiento, id_puesto) VALUES( :nombre, :nacimiento, :id_puesto )');
        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':nacimiento', $this->nacimiento);
        $stmt->bindValue(':id_puesto', $this->id_puesto);
        return $stmt->execute();
    }


}
